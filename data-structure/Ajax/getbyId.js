"use strict";

document.getElementById("getById").onclick = function() {

	var myobject = {
		EntryId: document.getElementById("EntryId").value
	}

	function get(object, callback) {

		var xhr = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry/' + document.getElementById("EntryId").value;

		xhr.open('GET', url);

		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

		xhr.addEventListener("load", function() {
			callback(xhr.responseText);

		});
		xhr.addEventListener('error', function() {

			console.log(xhr.status + ' : ' + xhr.statusText);
		});
		xhr.send();
	};


	get(myobject, function(gg) {
		document.getElementById('id').innerHTML = gg;
	});
	document.getElementById("EntryId").value = '';
}