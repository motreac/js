"use strict";
document.getElementById("put").onclick = function() {

	var email = {

		EntryId: document.getElementById("EntryId").value,
		Author: document.getElementById("Author").value,
		Title: document.getElementById("Title").value,
		Text: document.getElementById("Text").value
	};


	function put(obj, cb) {

		var xhr = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhr.open('PUT', url);

		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

		xhr.addEventListener("load", function() {
			cb(xhr.responseText);

		});
		xhr.addEventListener('error', function() {

			console.log(xhr.status + ' : ' + xhr.statusText);
		});
		xhr.send(JSON.stringify(obj));
	};

	put(email, function(gg) {
		document.getElementById("id").innerHTML = gg;
	});

	document.getElementById("EntryId").value = '';
	document.getElementById("Author").value = '';
	document.getElementById("Title").value = '';
	document.getElementById("Text").value = '';
}