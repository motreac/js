"use strict";

document.getElementById('get').onclick = function() {

	function get(callback) {

		var xhr = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhr.open('GET', url);

		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

		xhr.addEventListener("load", function() {
			callback(xhr.responseText);

		});
		xhr.addEventListener('error', function() {


			console.log(xhr.status + ' : ' + xhr.statusText);
		});
		xhr.send();
	};


	get(function(gg) {
		document.getElementById('id').innerHTML = gg;
	});

}