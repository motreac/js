"use strict";

document.getElementById('delete').onclick = function() {

	var email = {
		EntryId: document.getElementById('EntryId').value

	};

	function deletep(obj, cb) {

		var xhr = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhr.open('DELETE', url);

		xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

		xhr.addEventListener('load', function() {
			cb(xhr.responseText);

		});
		xhr.addEventListener('error', function() {

			console.log(xhr.status + ' : ' + xhr.statusText);
		});
		xhr.send(JSON.stringify(obj));
	};


	deletep(email, function(gg) {
		document.getElementById('id').innerHTML = gg;
	});

	document.getElementById('EntryId').value = '';
}