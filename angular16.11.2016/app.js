angular.module('pagination', []);

angular
	.module('pagination')
	.filter('StartFrom', [StartFrom]);

function StartFrom () {
	return function (input, start) {
		start = +start;
		return input.slice(start);
	};
}

angular
	.module('pagination')
	.controller('PaginationController', [PaginationController]);

function PaginationController () {
	var self = this;

	self.currentPage = 0;
	self.itemsOnPage = 12;

	self.collection = [
		{
			name: 'MAjor',
			id: 1,
			type: 'norm'

		},{
			name: 'lololo',
			id: 2,
			type: 'superCar'

		},{
			name: 'zacem',
			id: 3,
			type: 'extra'

		},{
			name: 'eto',
			id: 4,
			type: 'Nexus'

		},{
			name: 'qwerty',
			id: 5,
			type: '666'

		},{
			name: 'fwfwqfwqfqw',
			id: 6,
			type: 'Iphone'

		},{
			name: 'igewgwegwegwtem',
			id: 7,
			type: '1111'

		},{
			name: 'itherhrehrebem',
			id: 8,
			type: 'Nokia'

		},{
			name: 'itberrebrebreem',
			id: 9,
			type: 'rackets'

		},{
			name: 'iteberbreberm',
			id: 10,
			type: 'Xperia'

		},{
			name: 'itbrebrebreem',
			id: 11,
			type: 'team'

		},{
			name: 'berbrebre',
			id: 12,
			type: 'phone'

		},{
			name: '65666',
			id: 13,
			type: 'htc'

		},{
			name: '5555',
			id: 14,
			type: 'phone'

		},{
			name: '7777',
			id: 15,
			type: 'apple'

		},{
			name: '88888',
			id: 16,
			type: 'laptop'

		},
		{
			name: 'MAjor',
			id: 1,
			type: 'norm'

		},{
			name: 'lololo',
			id: 2,
			type: 'superCar'

		},{
			name: 'zacem',
			id: 3,
			type: 'extra'

		},{
			name: 'eto',
			id: 4,
			type: 'Nexus'

		},{
			name: 'qwerty',
			id: 5,
			type: '666'

		},{
			name: 'fwfwqfwqfqw',
			id: 6,
			type: 'Iphone'

		},{
			name: 'igewgwegwegwtem',
			id: 7,
			type: '1111'

		},{
			name: 'mmmmmmm',
			id: 8,
			type: 'Nokia'

		},{
			name: 'bbbbbbbbbb',
			id: 9,
			type: 'rackets'

		},{
			name: 'iteberbreberm',
			id: 10,
			type: 'Xperia'

		},
		{
			name: 'MAjor',
			id: 22,
			type: 'norm'

		},{
			name: 'lololo',
			id: 255,
			type: 'superCar'

		},{
			name: 'zacem',
			id: 365,
			type: 'extra'

		},{
			name: 'eto',
			id: 46546,
			type: 'Nexus'

		},{
			name: 'qwerty',
			id: 5656,
			type: 'nokia'

		},{
			name: 'fwfwqfwqfqw',
			id: 66546,
			type: 'aaaa'

		},{
			name: 'aaaaaaaa',
			id: 7654,
			type: '1111'

		},{
			name: 'bbbbbbb',
			id: 865465,
			type: 'mahmud'

		},{
			name: 'cccccc',
			id: 96546,
			type: 'rackets'

		},{
			name: 'iteberbreberm',
			id: 106564,
			type: 'Xperia'

		} 
	];

	self.nokias = 0;
	self.major = 0;
	self.numberOfCollection = self.collection.length;

	

	self.x = 'type'; //to find information by type in collection

	self.sort = function sort(item) {
	
		
		// self.reverse = (self.x === x ) ? !self.reverse : false;

			self.x = item;

			};

	// check if we are on the first page
	self.firstPage = function firstPage () {
		return self.currentPage == 0;
	};

	// check if we are on the last page
	self.lastPage = function lastPage () {
		var lastPageNum = Math.ceil(self.numberOfCollection/ self.itemsOnPage -1);
		
		return self.currentPage == lastPageNum;
	};

	// count how many pages are in the document
	self.numberOfPages = function numberOfPages () {
		return Math.ceil(self.numberOfCollection / self.itemsOnPage);
	};

	// starting point to display from the array
	self.startingItem = function startingItem () {
		return self.currentPage * self.itemsOnPage;
	};

	// move to the next page
	self.next = function next () {
		self.currentPage += 1;
	};

	// move to the previous page
	self.back = function back () {
		self.currentPage -= 1;
	};
}