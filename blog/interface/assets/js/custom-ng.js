var blog = angular.module("myBlog", ['ngRoute', 'UserValidation']);
blog.controller('userCtrl', function($scope) {
    $scope.fullName = '';
    $scope.login = '';
    $scope.passw1 = '';
    $scope.passw2 = '';
    $scope.clearForm = function () {
        $scope.fullName = '';
        $scope.login = '';
        $scope.passw1 = '';
        $scope.passw2 = '';
        $('#registerModal').modal('hide');

    };

    $scope.clearLoginForm = function () {
        $scope.login = '';
        $scope.passw1 = '';
        $('#loginModal').modal('hide');

    };
});



angular.module('UserValidation', []).directive('wjValidationError', function () {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctl) {
      scope.$watch(attrs['wjValidationError'], function (errorMsg) {
        elm[0].setCustomValidity(errorMsg);
        ctl.$setValidity('wjValidationError', errorMsg ? false : true);
      });
    }
  };
});

blog.config([ '$routeProvider', '$locationProvider', function($routeProvide, $locationProvider) {
    // $locationProvider.html5Mode({
    //     enabled: true
    // })
    $routeProvide

    .when('/', {
        templateUrl: 'template/home.html',
        controller: 'mainNewsCtrl'
    })
    .when('/login', {
        templateUrl: '/',
        controller: 'loginCtrl'
    })
    .when('/logout', {
        redirectTo: '/',
        controller: 'logoutCtrl'
    })
    .when('/dashboard', {

        resolve: {
            "check": function($location, $rootScope){
                if(!$rootScope.loggedIn) {
                    $location.path('/');
                }
            }

        },
        templateUrl: 'template/dashboard.html'


    })
    .otherwise({
        redirectTo: '/'
    });
}]);
blog.controller('registrationCtrl', ['$scope',  '$location', function($scope, $location) {
    $scope.regSubmit = function() {

            $location.path('/');

    }
}]);
blog.controller('mainNewsCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {

var url = 'http://localhost:3000/entry';
$http.get(url).success(function(data) {
    $scope.articles = data;
    console.log($scope.articles);

    console.log(localStorage);
});


}]);


blog.controller('loginCtrl', function($scope, $location, $rootScope){
    $scope.submit = function() {
        if(localStorage.lenght != 0) {
            console.log(localStorage.lenght == 0);
            $rootScope.loggedIn = true;
                        $location.path('/dashboard');
                        document.getElementById('logoutButton').style.display = 'block';
document.getElementById('registerButton').style.display = 'none';
document.getElementById('loginButton').style.display = 'none';
        } else {
            alert('You\'re not logged in!')
        }


    }
});


blog.controller('logoutCtrl', ['$scope',  '$location', function($scope, $location) {
    $scope.logOut = function() {


            $location.path('/');

    }
}]);
