-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: bloger
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `EntryId` varchar(44) NOT NULL,
  `Post_title` varchar(100) NOT NULL,
  `Post_text` text NOT NULL,
  `datePost` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`EntryId`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES ('EntryId_09e68cf5-aeb2-460e-ae36-87ec23bd8795','dsfgdfg','dsfgdfsgdsfg',1478094762980,8,0),('EntryId_100be153-1bba-4339-af03-04289968f028','Hello','HelloHelloHelloHelloHelloHello',1478095319033,3,0),('EntryId_16fbed45-5995-494d-973d-bc3f21048d18','Discover Greece in its fascinating way!','Travel today to discover more places...',1478180427949,22,0),('EntryId_17fcedda-bcb4-4942-8e5a-1f1575530af5','yyy','hgfhgfhgf',1478098419021,8,0),('EntryId_2116147c-958b-4a30-abc8-2d90c5c81d14','Manchester United won again!','Spectacular game was yesterday.',1478176506776,20,0),('EntryId_35e6cbec-e21e-4c21-8e69-204b9d51cd8c','dsfgdfg','dsfgdfsgdsfg',1478094765539,8,0),('EntryId_3a2d7c12-14e5-4ff5-9910-ee7857b63aea','My first post!','My first post!My first post!My first post!My first post!My first post!',1478180996117,20,0),('EntryId_41805b10-fe51-40d0-9a1f-21ea994a1cf7','Carolina','addpostaddpostaddpostaddpostaddpostaddpostaddpostaddpost',1478094985575,8,0),('EntryId_418c813e-f519-45ff-8b57-ad1b2408fa9a','yyy','hgfhgfhgf',1478098419544,8,0),('EntryId_4d3347eb-b7a3-45de-a7b0-008b5ed7d45b','yyy','hgfhgfhgf',1478098418869,8,0),('EntryId_56eb4a8f-a514-4aad-9778-bd0be5aa0941','Hello','Hello world',1478095285170,8,0),('EntryId_6b4364f4-56b4-4b5a-9013-d1479ce6050a','dsfgdfg','dsfgdfsgdsfg',1478094763283,8,0),('EntryId_74a58114-25bc-47cb-81ad-e1cdcc0e8021','Find your way to get over insomnia.','Find your way to get over insomnia.Find your way to get over insomnia.',1478181109279,20,0),('EntryId_7dc4aec2-1833-45d5-9e71-e049a94f1a3a','dsfgdfg','dsfgdfsgdsfg',1478094765061,8,0),('EntryId_7f01098e-c9d1-4407-9c77-b8a884948cd4','cristina','cristina',1478167134102,8,0),('EntryId_8c18af6d-328e-480b-8af9-f8a22db80067','Get your work done!','Stop procrastinating now!',1478180773720,22,0),('EntryId_8f170ac8-ad6f-4571-b69a-3fff6b94cb58','dsfgdfg','dsfgdfsgdsfg',1478094765219,8,0),('EntryId_93649cff-e9b5-4de1-9fe4-0fa83a39d240','dsfgdfg','dsfgdfsgdsfg',1478094765701,8,0),('EntryId_99ba202d-1b6c-4d6d-9619-60e2c76bc69b','Maria is beautiful.','Maria is beautiful. Maria is beautiful.Maria is beautiful.Maria is beautiful.',1478182841354,22,0),('EntryId_a0aa48b7-eced-4724-bed8-37ae0e8dc43c','yyy','hgfhgfhgf',1478098418643,8,0),('EntryId_a2191051-1491-41a1-9548-220ae40c98e5','dsfgdfg','dsfgdfsgdsfg',1478094763451,8,0),('EntryId_ac07367f-8680-4ec2-88c5-903edb4d1733','yyy','hgfhgfhgf',1478098412958,8,0),('EntryId_b2e36833-56ca-4585-9e4b-d02707a9e627','yyy','hgfhgfhgf',1478098419185,8,0),('EntryId_b4222ea9-4d3c-48d7-877b-88e579c95559','dsfgdfg','dsfgdfsgdsfg',1478094765371,8,0),('EntryId_bcba9caf-73ed-43e8-a57b-a47f7b8c5e74','hello world!','hello world!hello world!hello world!hello world!hello world!hello world!',1478165462394,8,0),('EntryId_d7ac2834-8c1f-4a03-a0e1-7eaeabb80c3f','dsfgdfg','dsfgdfsgdsfg',1478094763614,8,0),('EntryId_dae44ed4-a6e1-4675-8a77-16b97854cb37','How to start up oyur business?','Short 5 step plan for your future.',1478167022287,8,0),('EntryId_dca438f5-4a62-4c2f-8ded-8663ef57a9d4','How to learn Angular in 2 days.','Magical method to learn Angular in very short time!',1478096975398,8,0),('EntryId_e36ce8e6-35ff-49db-9ede-8e6a1212f056','yyy','hgfhgfhgf',1478098419853,8,0),('EntryId_e6a05d8d-179d-43f3-9224-4dada39d7fd5','yyy','hgfhgfhgf',1478098419374,8,0),('EntryId_f1119867-ebee-4b4e-a2cf-efd1a1f2e0d3','yyy','hgfhgfhgf',1478098419700,8,0),('EntryId_f25522f3-51b8-45f8-801e-dee02c190531','dsfgdfg','dsfgdfsgdsfg',1478094761685,8,0),('EntryId_f894858f-1970-4654-a683-53261cfd5043','hello Wolrd!','Today was a bad day.',1478185523388,22,0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-03 17:10:42
