// var MongoClient = require('../createDb.js')
var MongoClient = require('mongodb').MongoClient
var uuid = require('node-uuid');
assert = require('assert');

var url = 'mongodb://localhost:27017/myproject';

exports.getAllProducts = function getAllProducts (req, res) {

	MongoClient.connect(url , function (err, db ) {

		db.collection('todo').find().toArray( function (err, result) {
			if (err) throw err;

			res.send(result);
		});
	});

};

exports.addProduct = function addProduct (req, res) {

	MongoClient.connect(url , function (err, db ) {

		db.collection('todo').insert({id: uuid.v4(), name : req.body.name}, function (err, result) {
			if (err) throw err;
			res.send('ok');
		});
	});
};

exports.updatestatusProduct = function updatestatusProduct (req, res) {

console.log(req.params.id);
	if(!req.params.id) {
		res.status(401).send('Id wasn\'t found!' );
	}
	MongoClient.connect('mongodb://localhost:27017/myproject')
		.then(response => {
			response.collection('todo').findOneAndUpdate({
				id : req.params.id

			});
			return true;
		})
		.then(send => {
			res.send('Task deleted');
		})
	}

// exports.deleteProduct = function deleteProduct (req, res) {

// 	MongoClient.connect ( url, function (err, db) {

// 		db.collection('items').findOneAndDelete({id : req.params.id}, function (err, result) {
// 			if (err) throw err;
// 			res.send('success  delete');
// 		});
// 	});

// };
exports.deleteTodo = function deleteTodo (req, res) {
	console.log(req.params.id);
	if(!req.params.id) {
		res.status(401).send('Id wasn\'t found!' );
	}
	MongoClient.connect('mongodb://localhost:27017/myproject')
		.then(response => {
			response.collection('todo').findOneAndDelete({
				id : req.params.id
			});
			return true;
		})
		.then(send => {
			res.send('Task deleted');
		})
	}


exports.getProduct = function getProduct (req, res) {
	MongoClient.connect ( url, function (err, db) {

		db.collection('todo').findOne({name: req.params.id}, function (err, result) {

			if(err) throw err;

			res.send(result);
		});
	});

};