exports.routes = function routes (app) {

	var entry = require('./components/controller.js');

	 app.get('/products', entry.getAllProducts);
	 app.get('/products/:id', entry.getProduct);
	 app.post('/products', entry.addProduct);
	 // app.put('/products/:id', entry.updatestatusProduct);
	 app.delete('/products/:id', entry.deleteTodo);
};		