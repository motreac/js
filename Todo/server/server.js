var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var uuid = require('node-uuid');



app.use(bodyParser.json());
app.use(express.static(__dirname + '/../public'));

require('./routes.js').routes(app);

var server = app.listen('3333', function () {
	console.log('Example app listening on port 3333!');
});

