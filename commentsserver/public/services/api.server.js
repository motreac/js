(function() {
    'use strict';

    angular
        .module('app')
        .factory('Api', ['$http', 'ConfigService', Api]);

        function Api($http, ConfigService) {
            var api = {};

            // Get all products http service function
            api.get = function (url) {
                return $http.get(ConfigService.apiUrl + url);
            };

            // Add a new product http service function
            api.post = function (url, data) {
                return $http.post(ConfigService.apiUrl + url, data);
            };

            // Edit a product http service function
            api.put = function (url, id, data) {
                return $http.put(ConfigService.apiUrl + url + '/' + id, data)
            };

            // Delete a product http service function
            api.delete = function (url, id, index) {
                return $http.delete(ConfigService.apiUrl + url + '/' + id);
            }

            return api;
        }
}());
