(function() {
    'use strict';

    angular
        .module('app')
        .factory('ConfigService', [ConfigService]);

        function ConfigService() {
            var Config = {};

            Config.apiUrl = 'http://localhost:3333';

            return Config;
        }
}());
