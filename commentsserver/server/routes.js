/**
 * Created by hanni on 11/22/16.
 */
exports.routes = function routes (app) {

    var comments = require('./components/comments');

    app.get('/comments', comments.getAllComments);
    app.post('/comments', comments.addNewComment);

};