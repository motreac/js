(function(){
	'use strict';

	angular
		.module('products')
		.factory('Api', ['$http', 'ConfigService', Api]);

	function Api($http, ConfigService) {
		var api = {};

		api.get = function (url) {
			var request = $http.get(url);

			return request;
		};


		api.post = function (url, data) {
			var request = $http.post(url, data);

			return request;
		};

		api.delete = function (url) {
			var request = $http.delete(url);

			return request;
		};

		api.put = function (url, data) {
			var request = $http.put(url, data); 

			return request;
		};

		return api;
	}
}());