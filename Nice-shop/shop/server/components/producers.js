var fs = require('fs');
var Uuid = require('node-uuid');
var bodyParser = require('body-parser');

exports.getAllProducers = function getAllProducers(req, res) {
	var producersList = []; //array wich will contain producers after readinf

	//using file sistem - reading info stocked on server
	fs.readdir('./components/producers/', function(err, files) {
		if (err) {
			return res.status(401).send('Failed read directory with producers');
		}

		//making a loop over files and getting producers
		files.forEach(function(item) {
			var obj = fs.readFileSync('./components/producers/' + item, {
				encoding: 'UTF-8'
			});
			//decoding files|read content|parsing to json and pushing to result 
			producersList.push(JSON.parse(obj.toString()));
		});

		//control of producer list
		if (producersList.length === 0) {
			return res.status(404).send('Sorry, there are no producers in stock.');
		}

		res.send(producersList); //sending results
	});
};

exports.getProducersProducts = function getProducersProducts(req, res) {
	//if we havent received id send error
	if (!req.params.id) {
		return res.status(409).send('Name of producer is mandatory');
	}

	var results = []; //var wich will contain results
	var producerNameExists;

	//read all files from data directory
	fs.readdir('./components/data/', function(err, files) {
		if (err) {
			return res.status(401).send('Failed read directory with products');
		}

		//making a loop over files and getting producers)
		
		files.forEach(function(item) {

			//decoding files|read content|parsing to json and pushing to result 
			var obj = fs.readFileSync('./components/data/' + item, {
				encoding: 'UTF-8'
			});

			obj = JSON.parse(obj);
			if (obj.producer === req.params.id) {
				results.push(obj);
				producerNameExists = true;
			}
		});
		//verifying if obj with received name producer was found, if yes send results

		//if products was not found send error
		if (!producerNameExists) {
			return res.status(404).send('Not found such ID!');
		}
		console.log(results);
	return res.status(200).send(results);
	});
	
};

exports.addProducers = function addProducers(req, res) {
	//controll of blank property
	if (!req.body.name) {
		return res.status(409).send('Name of producer is mandatory');
	}
	
	var producerId = 'ProducerID_' + Uuid.v4(); // generating new id for new producer

	//creating an object wich will be pushed in data
	var entryData = req.body;

	// creting new property - id and
	//generating ID
	entryData.id = producerId;
	entryData = JSON.stringify(entryData);

	//writing a file json with entry data info
	fs.writeFile('./components/producers/' + producerId + '.json', entryData, function(err) {
		if (err) {
			return res.status(401).send('Error in creating file');
		}
	});

	// respons from server
	res.send('Producer with name: ' + req.body.name+' and ID '+ producerId + 
		' was added succesufully!');
};

exports.delProducers = function delProducers(req, res) {
	//controll of blank property
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}

	fs.unlink('./components/producers/' + req.params.id + '.json', function(err) {
		if (err) {
			return res.status(401).send('Error in deleting file');
		}

		return res.send('producer was succesufully deleted');
	});
};

exports.editProducers = function editProducers(req, res) {
	//if we havent received id send error
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}
	var producerIdExists;
	//read all files from data directory
	fs.readdir('./components/producers/', function(err, files) {
		if (err) {
			return res.status(401).send('Failed read directory with producers');
		}

		//making a loop over files and getting producers)
		files.forEach(function(item) {

			//decoding files|read content|parsing to json and pushing to result 
			var obj = fs.readFileSync('./components/producers/' + item, {
				encoding: 'UTF-8'
			});

			obj = JSON.parse(obj);

			// checking if our id is identical to current file id
			if (obj.id === req.params.id) {
				var input = req.body;
				input.id = req.params.id;
				fs.writeFile('./components/producers/' + item, JSON.stringify(input), function(err) { //if its true modifing the file
					if (err) {
						return (401, 'Failed to add modified properties');
					}
				});
				producerIdExists = true;
			}
		});

		//verifying if obj with received id was found, if yes send results
		if (producerIdExists) {
			return res.status(200).send('Producer properties was succesufully modified!');
		}

		//if id was not found send error
		if (!producerIdExists) {
			res.status(404).send('Not found such ID!');
		}
	});
};